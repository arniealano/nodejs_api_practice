const express = require('express');
const { register, login, getMe } = require('../controller/auth');
const { protect } = require('../middleware/auth');
const Router = express.Router();

Router.route('/register').post(register);
Router.route('/login').post(login);
Router.route('/me').get(protect, getMe)

module.exports = Router;