const express = require('express');
const { getBootcamps, createBootcamp, getBootcamp, updateBootcamp, deleteBootcamp, getBootcampsInRadius } = require('../controller/bootcamps');
const Router = express.Router();
const courseRouter = require('./course');
const bootcamps = require('../models/Bootcamps');
const advancedResults = require('../middleware/advancedResults');
const { protect } = require('../middleware/auth');

Router.route('/').get(advancedResults(bootcamps, 'courses'), getBootcamps).post(protect, createBootcamp);
Router.route('/radius/:countryCode/:zipcode/:distance').get(getBootcampsInRadius);
Router.route('/:id').get(getBootcamp).put(protect, updateBootcamp).delete(protect, deleteBootcamp);


Router.use('/:bootcampId/courses', courseRouter);

module.exports = Router;