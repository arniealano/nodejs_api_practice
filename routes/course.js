const express = require('express');
const { getCourses, getCourse, addCourse, updateCourse, deleteCourse } = require('../controller/course');
const Router = express.Router({mergeParams: true});
const courses = require('../models/Course');
const advancedResults = require('../middleware/advancedResults');
const { protect } = require('../middleware/auth');

Router.route('/').get(advancedResults(courses, {
    path: 'bootcamp',
    select: 'name description'
}),getCourses).post(protect, addCourse);
Router.route('/:id').get(getCourse).put(protect, updateCourse).delete(protect, deleteCourse);

module.exports = Router;