const Course = require('../models/Course');
const Bootcamp = require('../models/Bootcamps');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');

// @desc     Get courses
// @route    GET /api/v1/courses
// @route    GET /api/v1/bootcamps/:bootcampId/courses
// @acess    Public
exports.getCourses = asyncHandler(async (req, res, next) => {
    if(req.params.bootcampId){
        const courses = await Course.find( { bootcamp : req.params.bootcampId } );
        res.status(200).json({
            success: true,
            count: courses.length,
            data: courses
        })        
    }else{
        res.status(200).json(res.advancedResults);
    }



})

// @desc     Get courses
// @route    GET /api/v1/courses/:id
// @acess    Public
exports.getCourse = asyncHandler(async (req, res, next) => {
    const course = await Course.findById(req.params.id).populate({
        path: 'bootcamp',
        select: 'name description'
    });
    if(!course){
        return next(new ErrorResponse(`Course with id ${req.params.id} does not exist`, 404));
    }
    res.status(200).json({
        success: true,
        data: course
    })    
})

// @desc     Create courses
// @route    POST /api/v1/bootcamps/:bootcampId/courses
// @acess    Private
exports.addCourse = asyncHandler(async (req, res, next) => {
    req.body.bootcamp =  req.params.bootcampId;
    const bootcamp = await Bootcamp.findById(req.params.bootcampId);

    if(!bootcamp){
        return next(new ErrorResponse(`Bootcamp with id ${req.bootcampId.id} does not exist`, 404));        
    }

    const course =  await Course.create(req.body);

    res.status(200).json({
        success: true,
        data: course
    })    

})

// @desc     Update course
// @route    POST /api/v1/courses/:id
// @acess    Public
exports.updateCourse = asyncHandler(async (req, res, next) => {
    const course = await Course.findByIdAndUpdate(req.params.id, req.body, {
        runValidators: true,
        new:true        
    });

    if(!course){
        return next(new ErrorResponse(`Course with id ${req.params.id} does not exist`, 404));        
    }

    res.status(200).json({
        success: true,
        data: course
    })    

})

// @desc     Delete course
// @route    DELETE /api/v1/courses/:id
// @acess    Public
exports.deleteCourse = asyncHandler(async (req, res, next) => {
    const course = await Course.findById(req.params.id);

    if(!course){
        return next(new ErrorResponse(`Course with id ${req.params.id} does not exist`, 404));        
    }

    await course.remove();
    console.log(course);

    res.status(200).json({
        success: true,
        data: course
    })    

})