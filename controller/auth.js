const User = require('../models/User');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');
// @desc     Register User
// @route    POST /api/v1/auth/register
// @acess    Public
exports.register = asyncHandler( async (req, res, next) => {
    const {name, email, password, role} = req.body;
    const user = await User.create({
        name,
        email,
        password,
        role
    });

    const token = user.getSignJwtToken();
    res.status(200).json({
        success: true,
        token: token
    })

} )

// @desc     Login User
// @route    POST /api/v1/auth/login
// @acess    Public
exports.login = asyncHandler( async (req, res, next) => {

    const {email, password} = req.body;

    if(!email || !password){
        return next(new ErrorResponse('Please provide an email and password', 400))
    }

    const user = await User.findOne({email}).select('+password');

    if(!user){
        return next(new ErrorResponse('Invalid Credentials', 400))
    }

    const isMatched = await user.matchPassword(password);

    if(!isMatched){
        return next(new ErrorResponse('Invalid Credentials', 400))
    }

    sendTokenResponse(user, 200, res);
} )

// @desc     Get user details
// @route    GET /api/v1/auth/me
// @acess    Private
exports.getMe = asyncHandler( async (req, res, next) => {
    const user = await User.findById(req.user.id);
    res.status(200).json({
        success: true,
        data: user
    })
} )

const sendTokenResponse = (user, statusCode, res) => {
    const token = user.getSignJwtToken();
    const options = {
        expires: new Date(Date.now() + (process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000)),
        httpOnly: true

    };

    res.status(statusCode)
       .cookie('token', token, options)
       .json({
           success:true,
           token
       })
}