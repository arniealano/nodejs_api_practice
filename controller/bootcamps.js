const Bootcamps = require('../models/Bootcamps');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');
const geocoder = require('../utils/geocode');
// @desc     Get all bootcamps
// @route    GET /api/v1/bootcamps
// @acess    Public
exports.getBootcamps = asyncHandler(async (req, res, next) => {
        res.status(200).json(res.advancedResults);        
});

// @desc     Get bootcamp
// @route    GET /api/v1/bootcamps/:id
// @acess    Public
exports.getBootcamp = asyncHandler(async (req, res, next) => {
        const bootcamp = await Bootcamps.findById(req.params.id);
        if(!bootcamp){
            return next(new ErrorResponse(`Bootcamp with id ${req.params.id} does not exist`, 404));
        }
        res.status(200).json({
            success: true,
            data: bootcamp
        })
});

// @desc     Create new bootcamp
// @route    POST /api/v1/bootcamps
// @acess    Public
exports.createBootcamp = asyncHandler(async (req, res, next) => {
        const bootcamp = await Bootcamps.create(req.body);
        res.status(201).json({
            sucess: true,
            data: bootcamp
        })
});

// @desc     Update bootcamp
// @route    PUT /api/v1/bootcamps/:id
// @acess    Public
exports.updateBootcamp = asyncHandler(async (req, res, next) => {
        const bootcamp = await Bootcamps.findByIdAndUpdate(req.params.id, req.body, {
            runValidators: true,
            new:true
        })             
        if(!bootcamp){
          return next(new ErrorResponse(`Bootcamp with id ${req.params.id} does not exist`, 404));
        }
        res.status(200).json({
            success: true,
            data: bootcamp
        })
});

// @desc     Delete bootcamp
// @route    DELETE /api/v1/bootcamps/:id
// @acess    Public
exports.deleteBootcamp = asyncHandler(async (req, res, next) => { 
        const bootcamp = await Bootcamps.findById(req.params.id); 

        if(!bootcamp){
            return next(new ErrorResponse(`Bootcamp with id ${req.params.id} does not exist`, 404));
        }

        bootcamp.remove();
        
        res.status(200).json({
            success: true,
            data: bootcamp
        })   
});


// @desc     Get bootcamp within radius
// @route   GET /api/v1/bootcamps/radius/:countryCode/:zipcode/:distance
// @acess    Public
exports.getBootcampsInRadius = asyncHandler(async (req, res, next) => { 
    const {zipcode, countryCode, distance} = req.params;
    const loc = await geocoder.geocode(`${zipcode}, ${countryCode}`);

    const lat = loc[0].latitude;
    const lng = loc[0].longitude;
    const radius = distance/3963;

    const bootcamps = await Bootcamps.find( {
        location: { $geoWithin: { $centerSphere: [ [ lng, lat ], radius ] } }
      } )

    res.status(200).json({
        success: true,
        count: bootcamps.length,
        data: bootcamps
    })

});