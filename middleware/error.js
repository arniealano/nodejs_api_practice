const ErrorResponse = require('../utils/errorResponse');
// Custom error handler
const errorHandler = (err, req, res, next) => {
    let error = { ...err };
    error.message = err.message;

    if(err.name === 'CastError') {
        const message = `Resource with id ${err.value} does not exist`;
        error = new ErrorResponse(message, 404);
    }

    if(err.code === 11000){
        const message = `Resourse with name ${err.keyValue.name} already exist`;
        error = new ErrorResponse(message, 400);
    }

    if(err.name === 'ValidationError'){
        const message = Object.values(err.errors).map(val => val.message);
        error = new ErrorResponse(message, 400);
    }

    if(err.name === 'JsonWebTokenError'){
        const message = 'Not authorized to access this route';
        error = new ErrorResponse(message, 401);
    }

    res.status(error.statusCode || 500).json({
        success: false,
        error: error.message
    })
}
module.exports = errorHandler;